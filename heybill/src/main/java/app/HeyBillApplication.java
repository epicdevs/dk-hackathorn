package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HeyBillApplication {
    public static void main(String[] args) {
        SpringApplication.run(HeyBillApplication.class, args);
    }
}
