package app.controller;

import app.model.Bill;
import app.model.Item;
import app.repository.BillRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/bills")
public class BillController {
    private static String UPLOAD_PATH = System.getProperty("user.home");

    @Autowired
    private BillRepository billRepository;

    @RequestMapping(method = RequestMethod.POST)
    public String imageUpload(@RequestParam MultipartFile image) throws IOException {
        String dateTime = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        String uuid = dateTime + UUID.randomUUID().toString().replace("-", "");
        FileUtils.forceMkdir(new File(UPLOAD_PATH + "/heybill/images"));
        FileUtils.copyInputStreamToFile(image.getInputStream(), new File(UPLOAD_PATH + "/heybill/images/" + uuid));

        billRepository.save(new Bill(uuid));
        return uuid;
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Bill> list() {
        return billRepository.findAllDesc();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Bill select(@PathVariable String id) {
        return billRepository.findOne(id);
    }

    @RequestMapping(value = "/{id}/info", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateInfo(@PathVariable String id, @RequestBody Bill bill) {
        bill.setId(id);
        bill.calcPayAmount();
        for (Item item : bill.getItems()) {
            item.setBill(bill);
        }
        bill.setStatus("P");
        billRepository.save(bill);
    }

    @RequestMapping("/test")
    public String test() {
        return "Hello, World!";
    }
}
