package app.repository;

import app.model.Bill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BillRepository extends JpaRepository<Bill, String> {
    @Query("select b from Bill b order by b.id desc")
    public List<Bill> findAllDesc();

    @Query("select b from Bill b where b.status ='N'")
    public List<Bill> findUnprocessedBills();
}
